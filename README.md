# Supports de présentation FFDN

Ce dépôt contient des slides et autres supports de présentations faites
dans le cadre de la Fédération FDN.

## Licence

Par défaut, la licence de tous les contenus est **CC-BY-SA**.

## Schéma de nommage

Les supports sont rangés par année, puis sont nommés ainsi :

    <evenement>-<personne>-<sujet>-<mois><année>

Si le thème de la présentation est général et n'a pas de sujet précis
(en gros, ça parle de la Fédération FDN en général), on peut omettre le sujet :

    <evenement>-<personne>-<mois><année>
