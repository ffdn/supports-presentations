#! /usr/bin/env python3
# coding: Utf-8

# On va supposer que tout AS/flocon directement relié à un autre est
#  en mesure de lui servir de transitaire pour atteindre les autres.

# On commence par indiquer, pour chaque AS/flocon, le numéro de ses
# transitaires arrivés sur le réseau avant lui (plus faible numéro)

network = [
    [], #0
    [], #1
    [], #2
    [2], #3
    [], #4
    [], #5
    [2, 3, 4], #6
    [0], #7
    [4], #8
    [1, 5], #9
    [1, 9], #10
    [6, 7, 8, 10], #11
    [0, 7, 8], #12
    [3, 5], #13
    [3, 5, 10, 11, 13], #14
    [1, 7, 10], #15
    [2, 3], #16
    [13, 16], #17
    [5, 13, 17], #18
    [5, 9], #19
    [5, 18, 19], #20
    [1, 9, 19], #21
    [8, 12], #22
    [0, 15], #23
    [21, 19], #24
    [1, 15], #25
    [4, 2], #26
    [4, 22], #27
    [19, 20, 24], #28
    [18, 20], #29
    [1, 21, 25], #30
    [16, 17], #31
    [17, 18], #32
    [21, 24, 30], #33
    [2, 26], #34
    [29, 32], #35
    [24, 28], #36
    [3, 26], #37
    [17, 31, 32], #38
    [20, 28, 29], #39
    [0, 12], #40
    [0, 23], #41
]

# On laisse python remplir les relations dans l'autre sens.
#  (Ça évite les erreurs en vérifiant ça depuis le .xcf…)

for i in range(len(network)):
    for j in range(len(network)):
        if i in network[j] and not j in network[i]:
            network[i].append(j)

# On en déduit le tableau de qui peut joindre qui directement.

dists, inf = [], len(network)+1.0
for i in range(len(network)):
    dists.append([])
    for j in range(len(network)):
        dists[i].append(0.0 if i == j else 1.0 if j in network[i] else inf)

# À partir duquel on constitue les distances entre tous les AS.

for n in range(2, len(network)):
    for i in range(len(network)):
        for j in range(len(network)):
            for k in network[i]:
                if dists[k][j]+1 < dists[i][j]:
                    dists[i][j] = dists[k][j]+1

# Plus qu'à faire la moyenne de ce tableau :-)

print(sum(sum(d)/len(network) for d in dists)/len(network))
